function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Определяем глобальные переменные для холста и контекста
var canvas;
var context;

let x = 200;
let y = 520;

window.onload = function() {
  // Подготавливаем холст
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");

  var initLetItSnow = function() {
    (function() {
      var requestAnimationFrame =
        window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback) {
          window.setTimeout(callback, 1000 / 60);
        };
      window.requestAnimationFrame = requestAnimationFrame;
    })();

    var flakes = [],
      mX = -100,
      mY = -100;

    var flakeCount = 450;

    function snow() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      for (var i = 0; i < flakeCount; i++) {
        var flake = flakes[i],
          x = mX,
          y = mY,
          minDist = 250,
          x2 = flake.x,
          y2 = flake.y;

        var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
          dx = x2 - x,
          dy = y2 - y;

        if (dist < minDist) {
          var force = minDist / (dist * dist),
            xcomp = (x - x2) / dist,
            ycomp = (y - y2) / dist,
            deltaV = force;

          flake.velX -= deltaV * xcomp;
          flake.velY -= deltaV * ycomp;
        } else {
          flake.velX *= 0.98;
          if (flake.velY <= flake.speed) {
            flake.velY = flake.speed;
          }
          flake.velX += Math.cos((flake.step += 0.05)) * flake.stepSize;
        }

        ctx.fillStyle = "rgba(255,255,255," + flake.opacity + ")";
        flake.y += flake.velY;
        flake.x += flake.velX;

        if (flake.y >= canvas.height || flake.y <= 0) {
          reset(flake);
        }

        if (flake.x >= canvas.width || flake.x <= 0) {
          reset(flake);
        }

        ctx.beginPath();
        ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
        ctx.fill();
      }
      requestAnimationFrame(snow);
    }

    function reset(flake) {
      flake.x = Math.floor(Math.random() * canvas.width);
      flake.y = 0;
      flake.size = Math.random() * 3 + 2;
      flake.speed = Math.random() * 1 + 0.5;
      flake.velY = flake.speed;
      flake.velX = 0;
      flake.opacity = Math.random() * 0.5 + 0.3;
    }

    function init() {
      for (var i = 0; i < flakeCount; i++) {
        var x = Math.floor(Math.random() * canvas.width),
          y = Math.floor(Math.random() * canvas.height),
          size = Math.random() * 3 + 4,
          speed = Math.random() * 1 + 0.5,
          opacity = Math.random() * 0.5 + 0.3;

        flakes.push({
          speed: speed,
          velY: speed,
          velX: 0,
          x: x,
          y: y,
          size: size,
          stepSize: Math.random() / 160,
          step: 0,
          opacity: opacity
        });
      }

      snow();
    }

    init();
  };
  initLetItSnow();

  canvas1 = document.getElementById("canvas1");
  ctx1 = canvas1.getContext("2d");
  const renderHero = () => {
    //отрисовка изображений
    var imgWe = new Image();
    imgWe.onload = function() {
      ctx1.drawImage(imgWe, x, y);
    };
    imgWe.src = "assets/777.png";

    window.onkeydown = processKey;

    function processKey(e) {
      // Если нажата стрелка влево, начинаем двигаться влево
      if (e.keyCode == 37) {
        dx = -10;
      }

      // Если нажата стрелка вправо, начинаем двигаться вправо
      if (e.keyCode == 39) {
        dx = 10;
      }
      if (x + dx <= 0 || x + dx >= 790) {
        return;
      }
      redraw(dx);
    }

    const redraw = dx => {
      var imgWe = new Image();
      imgWe.onload = function() {
        ctx1.clearRect(x, y, x + 215, y + 250);
        x = x + dx;
        ctx1.drawImage(imgWe, x, y);
      };
      imgWe.src = "assets/7777.png";
    };
  };

  let count = 0;
  let statistic = document.getElementById("statistic");
  statistic.style.color = "green";
  statistic.innerHTML = "Новогоднее настроение: " + count + "%";
  let toyx = getRandom(50, 900);
  let toyy = 10;
  let contact = false;
  number = getRandom(1, 2);
  const renderToys = () => {
    const drawToys = () => {
      if (toyx + 31 > x && toyx <= x + 215 && (toyy >= 487 && toyy <= 700)) {
        contact = true;
        count += 20;
        statistic.innerHTML = "Новогоднее настроение: " + count + "%";
      }
      if (count == 100) {
        end();
      }
      if (contact == true || toyy >= 700) {
        number = getRandom(1, 2);
        ctx1.clearRect(toyx, toyy, 31, 36);
        contact = false;
        toyy = 0;
        toyx = getRandom(50, 900);
      }
      var toy = new Image();
      toy.onload = function() {
        ctx1.clearRect(toyx, toyy, 31, 15);
        toyy = toyy + 1;
        ctx1.drawImage(toy, toyx, toyy);
      };
      toy.src = `assets/toy${number}.png`;
    };
    drawToys();
  };
  let timer = setInterval(renderToys, 1);
  renderHero();
  const end = () => {
    canvas.style.display = "none";
    canvas1.style.display = "none";
    statistic.style.display = "none";
    document.getElementById("congratulation").style.display = "block";
  };
};
